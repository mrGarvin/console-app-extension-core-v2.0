﻿using System;
using ConsoleAppExtensionCore.Interfaces;

namespace ConsoleAppExtensionCore.Test.Mocks
{
    public class ConsoleInformationMock : IConsoleInformation
    {
        #region Mock private fields

        private int _cursorLeft;
        private int _cursorTop;

        #endregion

        public int BufferWidth { get; }

        public int CursorLeft
        {
            get => _cursorLeft;
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value), value, "Cannot be less than 0.");
                if (value >= BufferWidth)
                    throw new ArgumentOutOfRangeException(nameof(value), value, $"Cannot be equal to or greater than {nameof(BufferWidth)}.");
                _cursorLeft = value;
            }
        }

        public int CursorTop
        {
            get => _cursorTop;
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value), value, "Cannot be less than 0.");
                _cursorTop = value;
            }
        }

        public ConsoleInformationMock(int bufferWidth)
        {
            BufferWidth = bufferWidth;
        }
    }
}