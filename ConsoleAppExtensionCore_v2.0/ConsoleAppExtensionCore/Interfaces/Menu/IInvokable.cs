﻿using ConsoleAppExtensionCore.Utilities.Menu;

namespace ConsoleAppExtensionCore.Interfaces.Menu
{
    public interface IInvokable
    {
        PostInvokeCommand Invoke();
    }
}