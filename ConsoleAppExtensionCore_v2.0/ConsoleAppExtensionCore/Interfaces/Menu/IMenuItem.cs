﻿namespace ConsoleAppExtensionCore.Interfaces.Menu
{
    public interface IMenuItem
    {
        string Label { get; }
    }
}