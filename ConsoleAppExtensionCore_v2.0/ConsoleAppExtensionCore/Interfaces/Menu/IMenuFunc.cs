﻿namespace ConsoleAppExtensionCore.Interfaces.Menu
{
    public interface IMenuFunc : INonInvokableMenuFunc, IInvokable
    {
        
    }
}