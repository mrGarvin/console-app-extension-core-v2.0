﻿using ConsoleAppExtensionCore.Utilities.Menu;

namespace ConsoleAppExtensionCore.Interfaces.Menu
{
    public interface IMenuOption : IMenuOption<IMenuItem>
    {
    }

    public interface IMenuOption<out T>
    {
        MenuOptionKey Key { get; }
        T Item { get; }
    }
}