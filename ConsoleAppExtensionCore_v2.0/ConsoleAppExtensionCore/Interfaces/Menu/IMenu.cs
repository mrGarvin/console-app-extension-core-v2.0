﻿namespace ConsoleAppExtensionCore.Interfaces.Menu
{
    public interface IMenu : INonInvokableMenu, IInvokable
    {
        void InvokeAsRootMenu();
    }
}