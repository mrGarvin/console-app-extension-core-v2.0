﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ConsoleAppExtensionCore.Interfaces.Menu;
using ConsoleAppExtensionCore.Utilities;
using ConsoleAppExtensionCore.Utilities.Input;
using ConsoleAppExtensionCore.Utilities.Menu;

namespace ConsoleAppExtensionCore
{
    public abstract class ConsoleExtensionCore
    {
        private static string DefaultProgramHeader
        {
            get
            {
                try
                {
                    return $" {Assembly.GetEntryAssembly().GetName().Name} ";
                }
                catch
                {
                    return " Program ";
                }
            }
        }

        private readonly IMenu _rootMenu;
        private readonly IMenuOption<IMenuFunc> _quitOption;

        protected ConsoleExtensionCore()
            : this("Main menu")
        {
        }

        protected ConsoleExtensionCore(string rootMenuLabel)
            : this(rootMenuLabel, DefaultProgramHeader)
        {
        }

        protected ConsoleExtensionCore(string rootMenuLabel, string programHeader)
        {
            _rootMenu = new Menu(rootMenuLabel, true);
            _quitOption = CreateQuitOption();
            _rootMenu.AddOptions(_quitOption);
            SetProgramHeader();
            Initialize();
            _rootMenu.InvokeAsRootMenu();

            IMenuOption<IMenuFunc> CreateQuitOption()
            {
                IMenu quitMenu = CreateYesOrNoMenu(
                    "Do you want to quit?",
                    () => { },
                    () => { },
                    PostInvokeCommand.ExitRootMenu
                );
                return CreateMenuFuncOption(MenuOptionKey.Q, "Quit", quitMenu.Invoke);
            }

            void SetProgramHeader()
            {
                ConditionValidator.ThrowArgumentNullExceptionFor(programHeader);
                ConditionValidator.ThrowArgumentExceptionIf(programHeader == string.Empty, "Cannot be empty", nameof(programHeader));
                Print.Settings.ProgramName = programHeader;
                MenuOptionKeyService.ProgramHeaderPrinter = () => Print.Header(programHeader);
            }
        }

        protected abstract void Initialize();
        
        protected void AddOptions(params IMenuOption<IMenuItem>[] options)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(options);
            ConditionValidator.ThrowInvalidOperationExceptionIf(options.Any(o => o.Key == MenuOptionKey.Q),
                $"{MenuOptionKey.Q} is reserved for the 'Quit' option.");
            _rootMenu.RemoveOptionsBy(MenuOptionKey.Q);
            AddOptionsTo(_rootMenu, options);
            _rootMenu.AddOptions(_quitOption);
        }

        protected void RemoveOptionsBy(params MenuOptionKey[] keys)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(keys);
            ConditionValidator.ThrowInvalidOperationExceptionIf(keys.Contains(MenuOptionKey.Q),
                "Cannot remove the 'Quit' option.");
            _rootMenu.RemoveOptionsBy(keys);
        }

        protected INonInvokableMenu GetSubmenuBy(MenuOptionKey key) => _rootMenu.GetSubmenuBy(key);

        protected INonInvokableMenuFunc GetMenuFuncBy(MenuOptionKey key) => _rootMenu.GetMenuFuncBy(key);

        protected bool Confirm(string question, bool trueAsDefault = true)
        {
            return GetConfirmationOf(question, trueAsDefault, false).Value;
        }

        protected bool? TryToConfirm(string question, bool trueAsDefault = true)
        {
            return GetConfirmationOf(question, trueAsDefault, true);
        }

        protected IMenu CreateNewSubMenu(string label, params IMenuOption<IMenuItem>[] menuOptions)
        {
            IMenu menu = new Menu(label, false);
            AddOptionsTo(menu, menuOptions);
            return menu;
        }

        protected IMenuOption<IMenu> CreateSubMenuOption(MenuOptionKey key, IMenu menu)
        {
            return new MenuOption<IMenu>(key, menu);
        }

        protected IMenuOption<IMenuFunc> CreateMenuFuncOption(MenuOptionKey key, string label, Func<PostInvokeCommand> func)
        {
            return new MenuOption<IMenuFunc>(key, new MenuFunc(label, func));
        }

        protected IMenu CreateYesOrNoMenu(string label, Action yesAction, Action noAction, PostInvokeCommand postInvokeCommand = PostInvokeCommand.ExitSubmenu)
        {
            IMenuOption<IMenuFunc> yesOption = CreateMenuFuncOption(MenuOptionKey.Y, "Yes", YesFunc);
            IMenuOption<IMenuFunc> noOption = CreateMenuFuncOption(MenuOptionKey.N, "No", NoFunc);
            Menu yesNoMenu = new Menu(label, yesOption, noOption);
            return yesNoMenu;

            PostInvokeCommand YesFunc()
            {
                yesAction();
                return postInvokeCommand;
            }

            PostInvokeCommand NoFunc()
            {
                noAction();
                return PostInvokeCommand.ExitSubmenu;
            }
        }

        private bool? GetConfirmationOf(string question, bool trueAsDefault, bool allowNull)
        {
            question += $" ({(trueAsDefault ? "Y/n" : "y/N")}): ";
            return allowNull ? InputService.GetNullableBool(question, trueAsDefault) : InputService.GetBool(question, trueAsDefault);
        }

        private void AddOptionsTo(IMenu menu, params IMenuOption<IMenuItem>[] menuOptions)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(menuOptions);
            foreach (IMenuOption<IMenuItem> menuOption in menuOptions)
            {
                switch (menuOption)
                {
                    case IMenuOption<IMenu> subMenu:
                        menu.AddOptions(subMenu);
                        break;
                    case IMenuOption<IMenuFunc> menuFunc:
                        menu.AddOptions(menuFunc);
                        break;
                    default:
                        throw new InvalidOperationException($"{nameof(menuOption)} is null or of invalid type.");
                }
            }
        }
    }
}