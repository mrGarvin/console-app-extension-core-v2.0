﻿using System;
using System.Collections;

namespace ConsoleAppExtensionCore.Utilities
{
    public static class ConditionValidator
    {
        public static void ThrowArgumentNullExceptionFor(params object[] objects)
        {
            if (objects is null)
                throw new InvalidOperationException($"{nameof(objects)} cannot be null.");
            foreach (object obj in objects)
                if (obj is null)
                    throw new ArgumentNullException(nameof(obj));
        }

        public static void ThrowArgumentNullExceptionForAllIn(params IEnumerable[] enumerables)
        {
            if (enumerables is null)
                throw new InvalidOperationException($"{nameof(enumerables)} cannot be null.");
            foreach (IEnumerable enumerable in enumerables)
            {
                foreach (object obj in enumerable)
                    if (obj is null)
                        throw new ArgumentNullException(nameof(obj));
            }
        }

        public static void ThrowArgumentOutOfRangeExceptionIf<T>(T value, ComparisonType comparisonType, T limit,
                                                                 string paramName = null, string message = null)
            where T : IComparable<T>
        {
            int comparisonResult = value.CompareTo(limit);
            switch (comparisonType)
            {
                case ComparisonType.EqualTo:
                    if (comparisonResult == 0)
                        break;
                    return;
                case ComparisonType.NotEqualTo:
                    if (comparisonResult != 0)
                        break;
                    return;
                case ComparisonType.LessThan:
                    if (comparisonResult < 0)
                        break;
                    return;
                case ComparisonType.LessThanOrEqualTo:
                    if (comparisonResult <= 0)
                        break;
                    return;
                case ComparisonType.GreaterThan:
                    if (comparisonResult > 0)
                        break;
                    return;
                case ComparisonType.GreaterThanOrEqualTo:
                    if (comparisonResult >= 0)
                        break;
                    return;
            }

            if (paramName is null)
                paramName = nameof(value);
            if (message is null)
                message = $"Cannot be {comparisonType.ToCleanString()} {limit}.";
            throw new ArgumentOutOfRangeException(paramName, value, message);
        }

        public static void ThrowInvalidOperationExceptionIf(bool throwIfTrue, string message, Exception innerException = null)
        {
            if (throwIfTrue)
                throw new InvalidOperationException(message, innerException);
        }

        public static void ThrowArgumentExceptionIf(bool throwIfTrue, string message, string paramName, Exception innerException = null)
        {
            if (throwIfTrue)
                throw new ArgumentException(message, paramName, innerException);
        }

        public static void ThrowExceptionIf(bool throwIfTrue, string message, Exception innerException = null)
        {
            if (throwIfTrue)
                throw new Exception(message, innerException);
        }
    }

    public enum ComparisonType
    {
        EqualTo,
        NotEqualTo,
        LessThan,
        LessThanOrEqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
    }

    public static class ComparisonTypeExtensions
    {
        public static string ToCleanString(this ComparisonType comparisonType)
        {
            switch (comparisonType)
            {
                case ComparisonType.EqualTo:
                    return "equal to";
                case ComparisonType.NotEqualTo:
                    return "not equal to";
                case ComparisonType.LessThan:
                    return "less than";
                case ComparisonType.LessThanOrEqualTo:
                    return "less than or equal to";
                case ComparisonType.GreaterThan:
                    return "greater than";
                case ComparisonType.GreaterThanOrEqualTo:
                    return "greater than or equal to";
                default:
                    throw new ArgumentOutOfRangeException(nameof(comparisonType), comparisonType, null);
            }
        }
    }
}