﻿using System;
using System.Collections.Generic;
using ConsoleAppExtensionCore.Interfaces.Menu;

namespace ConsoleAppExtensionCore.Utilities.Menu
{
    public class MenuOptionsContainer : IMenuOptionsContainer
    {
        private readonly IDictionary<MenuOptionKey, IMenu> _submenuMapper = new Dictionary<MenuOptionKey, IMenu>();
        private readonly IDictionary<MenuOptionKey, IMenuFunc> _menuFuncMapper = new Dictionary<MenuOptionKey, IMenuFunc>();

        public IInvokable this[MenuOptionKey key]
        {
            get
            {
                if (_submenuMapper.TryGetValue(key, out IMenu value))
                    return value;
                return _menuFuncMapper[key];
            }
        }

        public int Count => _submenuMapper.Count + _menuFuncMapper.Count;

        public void Add<T>(IMenuOption<T> menuOption) where T : IInvokable, IMenuItem
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(menuOption);
            AddToMapper(menuOption.Key, menuOption.Item);
        }

        public void AddRange<T>(IEnumerable<IMenuOption<T>> menuOptions) where T : IInvokable, IMenuItem
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(menuOptions);
            foreach (IMenuOption<T> menuOption in menuOptions)
                Add(menuOption);
        }

        public void RemoveBy(MenuOptionKey key)
        {
            if (_submenuMapper.ContainsKey(key))
                _submenuMapper.Remove(key);
            else
                _menuFuncMapper.Remove(key);
        }

        public IEnumerable<IMenuOption> ToReadOnly()
        {
            List<IMenuOption> readOnly = new List<IMenuOption>();
            foreach (KeyValuePair<MenuOptionKey, IMenu> submenuOption in _submenuMapper)
                readOnly.Add(new MenuOption(submenuOption.Key, submenuOption.Value));
            foreach (KeyValuePair<MenuOptionKey, IMenuFunc> menuFuncOption in _menuFuncMapper)
                readOnly.Add(new MenuOption(menuFuncOption.Key, menuFuncOption.Value));
            return readOnly;
        }

        private void AddToMapper<T>(MenuOptionKey key, T item) where T : IInvokable, IMenuItem
        {
            if (_submenuMapper.ContainsKey(key) || _menuFuncMapper.ContainsKey(key))
                throw new InvalidOperationException($"An {nameof(IMenuOption)} is already mapped to that key.");
            switch (item)
            {
                case IMenu _:
                    _submenuMapper.Add(key, (IMenu)item);
                    break;
                case IMenuFunc _:
                    _menuFuncMapper.Add(key, (IMenuFunc)item);
                    break;
                default:
                    throw new InvalidOperationException($"{typeof(T)} is not a valid type.");
            }
        }
    }
}