﻿using ConsoleAppExtensionCore.Interfaces.Menu;

namespace ConsoleAppExtensionCore.Utilities.Menu
{
    internal class MenuOption : IMenuOption
    {
        public MenuOptionKey Key { get; }
        public IMenuItem Item { get; }

        public MenuOption(MenuOptionKey key, IMenuItem item)
        {
            Key = key;
            Item = item;
        }
    }

    public class MenuOption<T> : IMenuOption<T>
        where T : IInvokable, IMenuItem
    {
        public MenuOptionKey Key { get; }
        public T Item { get; }

        public MenuOption(MenuOptionKey key, T item)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(item);
            Key = key;
            Item = item;
        }
    }
}