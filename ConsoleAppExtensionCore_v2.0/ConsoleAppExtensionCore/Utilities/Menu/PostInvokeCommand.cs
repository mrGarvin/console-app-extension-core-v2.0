﻿namespace ConsoleAppExtensionCore.Utilities.Menu
{
    public enum PostInvokeCommand
    {
        StayInSubmenu,
        ExitSubmenu,
        ExitRootMenu
    }
}