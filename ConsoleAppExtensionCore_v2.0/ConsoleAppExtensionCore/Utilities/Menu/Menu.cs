﻿using System;
using System.Collections.Generic;
using ConsoleAppExtensionCore.Interfaces.Menu;

namespace ConsoleAppExtensionCore.Utilities.Menu
{
    public class Menu : IMenu
    {
        private readonly IMenuOptionsContainer _optionses = new MenuOptionsContainer();
        private PostInvokeCommand _emptyMenuReturnCommand;

        public IMenuItem this[MenuOptionKey key] => _optionses[key] as IMenuItem;

        public string Label { get; }

        public int OptionCount => _optionses.Count;
        public bool IsEmpty { get; private set; }

        private Menu(string label)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(label);
            ConditionValidator.ThrowArgumentExceptionIf(label.Length == 0, "Cannot be empty.", nameof(label));
            Label = label;
        }

        public Menu(string label, bool isRootMenu)
            : this(label)
        {
            SetEmptyMenuReturnCommand(isRootMenu);
            AddEmptyMenuOption();
        }

        public Menu(string label, params IMenuOption<IMenu>[] subMenuOptions)
            : this(label, subMenuOptions, null) { }

        public Menu(string label, params IMenuOption<IMenuFunc>[] menuFuncOptions)
            : this(label, null, menuFuncOptions) { }

        public Menu(string label, IEnumerable<IMenuOption<IMenu>> submenuOptions, IEnumerable<IMenuOption<IMenuFunc>> menuFuncOptions)
            : this(label)
        {
            ConditionValidator.ThrowInvalidOperationExceptionIf(submenuOptions is null && menuFuncOptions is null,
                $"Both {nameof(submenuOptions)} and {nameof(menuFuncOptions)} cannot be null.");
            if (submenuOptions != null)
                _optionses.AddRange(submenuOptions);
            if (menuFuncOptions != null)
                _optionses.AddRange(menuFuncOptions);
        }
        
        private void AddEmptyMenuOption()
        {
            MenuOption<IMenuFunc> menuIsEmptyOption = new MenuOption<IMenuFunc>(MenuOptionKey.E, new MenuFunc("Menu is empty", () => _emptyMenuReturnCommand));
            _optionses.Add(menuIsEmptyOption);
            IsEmpty = true;
        }

        private void RemoveEmptyMenuOption()
        {
            _optionses.RemoveBy(MenuOptionKey.E);
            IsEmpty = false;
        }

        private void SetEmptyMenuReturnCommand(bool isRootMenu)
        {
            _emptyMenuReturnCommand = isRootMenu ? PostInvokeCommand.ExitRootMenu : PostInvokeCommand.ExitSubmenu;
        }

        public void AddOptions<T>(params IMenuOption<T>[] options)
            where T : IInvokable, IMenuItem
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(options);
            if (IsEmpty)
                RemoveEmptyMenuOption();
            foreach (IMenuOption<T> option in options)
                _optionses.Add(option);
        }

        public void RemoveOptionsBy(params MenuOptionKey[] keys)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(keys);
            foreach (MenuOptionKey key in keys)
            {
                _optionses.RemoveBy(key);
            }
            if (IsEmpty)
                AddEmptyMenuOption();
        }

        public INonInvokableMenu GetSubmenuBy(MenuOptionKey key) => this[key] as INonInvokableMenu;

        public INonInvokableMenuFunc GetMenuFuncBy(MenuOptionKey key) => this[key] as INonInvokableMenuFunc;

        public PostInvokeCommand Invoke()
        {
            SetEmptyMenuReturnCommand(false);
            MenuOptionKey? selectedOption = null;
            while (true)
            {
                selectedOption = MenuOptionKeyService.GetOptionKey(Label, _optionses.ToReadOnly(), selectedOption, true);
                if (selectedOption is null)
                    return PostInvokeCommand.StayInSubmenu;
                switch (_optionses[selectedOption.Value].Invoke())
                {
                    case PostInvokeCommand.StayInSubmenu:
                        continue;
                    case PostInvokeCommand.ExitSubmenu:
                        return PostInvokeCommand.StayInSubmenu;
                    case PostInvokeCommand.ExitRootMenu:
                        return PostInvokeCommand.ExitRootMenu;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void InvokeAsRootMenu()
        {
            SetEmptyMenuReturnCommand(true);
            MenuOptionKey? selectedOption = null;
            while (true)
            {
                selectedOption = MenuOptionKeyService.GetOptionKey(Label, _optionses.ToReadOnly(), selectedOption, false);
                if (_optionses[selectedOption.Value].Invoke() == PostInvokeCommand.ExitRootMenu)
                    return;
            }
        }
    }
}