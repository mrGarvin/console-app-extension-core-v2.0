﻿using System;
using ConsoleAppExtensionCore.Interfaces.Menu;

namespace ConsoleAppExtensionCore.Utilities.Menu
{
    public class MenuFunc : IMenuFunc
    {
        private readonly Func<PostInvokeCommand> _func;

        public string Label { get; }
        
        public MenuFunc(string label, Func<PostInvokeCommand> func)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(label, func);
            ConditionValidator.ThrowArgumentExceptionIf(label.Length == 0, "Cannot be empty.", nameof(label));
            Label = label;
            _func = func;
        }

        public PostInvokeCommand Invoke() => _func.Invoke();
    }
}