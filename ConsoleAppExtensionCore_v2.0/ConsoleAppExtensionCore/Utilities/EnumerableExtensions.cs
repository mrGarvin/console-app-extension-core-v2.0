﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleAppExtensionCore.Utilities
{
    public static class EnumerableExtensions
    {
        public static string EnumerableToString<T>(this IEnumerable<T> enumerable)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (T obj in enumerable)
                stringBuilder.Append(obj);
            return stringBuilder.ToString();
        }

        public static string EnumerableRangeToString<T>(this IEnumerable<T> enumerable, int index, int count)
        {
            T[] array = enumerable as T[] ?? enumerable.ToArray();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = index; i < index + count; i++)
                stringBuilder.Append(array[i]);
            return stringBuilder.ToString();
        }

        public static bool IsEmpty<T>(this IEnumerable<T> enumerable) => !enumerable.Any();
    }
}