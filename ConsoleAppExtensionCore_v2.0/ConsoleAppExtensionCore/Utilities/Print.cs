﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static System.Console;

namespace ConsoleAppExtensionCore.Utilities
{
    public static class Print
    {
        private static (int Top, int Left) _storedPosition;

        public static PrintSettings Settings { get; } = new PrintSettings();

        static Print()
        {
            Settings.ResetToDefaultColors();
        }

        public static void Enumerable(IEnumerable<object> enumerable, ObjectSeperator objectSeperator, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Enumerable(enumerable, objectSeperator.ToCleanString(), postPrintOption);
        }

        public static void Enumerable(IEnumerable<object> enumerable, string objectSeperator = "\n", PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(objectSeperator);

            if (enumerable is null)
            {
                Object(Settings.StringToPrintIfEnuamerableIsNull, PostPrintOption.PrintNewLine);
                return;
            }
            if (!enumerable.Any())
            {
                Object(Settings.StringToPrintIfEnuamerableIsEmpty, PostPrintOption.PrintNewLine);
                return;
            }

            object[] objects = enumerable as object[] ?? enumerable.ToArray();
            int lastIndex = objects.Length - 1;
            
            Settings.ToggleCursorVisibility();

            for (int index = 0; index <= lastIndex; index++)
            {
                if (index != lastIndex)
                    Object(objects[index] + objectSeperator, PostPrintOption.None);
                else
                    Object(objects[index], postPrintOption);
            }

            Settings.ToggleCursorVisibility();
        }

        public static void Objects(params object[] objects)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(objects as object);

            Enumerable(objects, ObjectSeperator.NewLine, PostPrintOption.PrintNewLine);
        }

        public static void Objects(PostPrintOption postPrintOption, params object[] objects)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(objects as object);

            Enumerable(objects, ObjectSeperator.NewLine, postPrintOption);
        }

        public static void Objects(ObjectSeperator objectSeperator, params object[] objects)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(objects as object);

            Enumerable(objects, objectSeperator, PostPrintOption.PrintNewLine);
        }

        public static void Objects(ObjectSeperator objectSeperator, PostPrintOption postPrintOption, params object[] objects)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(objects as object);

            Enumerable(objects, objectSeperator, postPrintOption);
        }

        public static void Object(object obj, PostPrintOption postPrintOption = PostPrintOption.None)
        {
            string output = obj is null ? Settings.StringToPrintIfObjectIsNull : obj.ToString();
            switch (postPrintOption)
            {
                case PostPrintOption.None:
                    Write(output);
                    break;
                case PostPrintOption.PrintNewLine:
                    WriteLine(output);
                    break;
                case PostPrintOption.WaitForKeyPress:
                    Write(output);
                    ReadKey(true);
                    break;
                case PostPrintOption.PrintNewLineAndWaitForKeyPress:
                    WriteLine(output);
                    ReadKey(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(postPrintOption), postPrintOption, null);
            }
        }

        public static void ReturningToMenu(string message = null, MessageType messageType = MessageType.Basic)
        {
            if (!(message is null))
            {
                switch (messageType)
                {
                    case MessageType.Basic:
                        Message(message, PostPrintOption.PrintNewLine);
                        break;
                    case MessageType.Success:
                        Success(message, PostPrintOption.PrintNewLine);
                        break;
                    case MessageType.Warning:
                        Warning(message, PostPrintOption.PrintNewLine);
                        break;
                    case MessageType.Error:
                        Error(message, PostPrintOption.PrintNewLine);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(messageType), messageType, null);
                }
            }
            Message("Returning to menu...", PostPrintOption.WaitForKeyPress);
        }

        public static void Success(string message, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Message(message, MessageType.Success, postPrintOption);
        }

        public static void Warning(string message, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Message(message, MessageType.Warning, postPrintOption);
        }

        public static void Warning(string message, Exception exception, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Warning(message, PostPrintOption.PrintNewLine);
            Exception(exception, postPrintOption);
        }

        public static void Error(string message, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Message(message, MessageType.Error, postPrintOption);
        }

        public static void Error(string mesage, Exception exception, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            Error(mesage, PostPrintOption.PrintNewLine);
            Exception(exception, postPrintOption);
        }

        private static void Message(string message, MessageType messageType, PostPrintOption postPrintOption)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(message);
            switch (messageType)
            {
                case MessageType.Success:
                    Settings.SetSuccessColors();
                    break;
                case MessageType.Warning:
                    Settings.SetWarningColors();
                    break;
                case MessageType.Error:
                    Settings.SetErrorColors();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(messageType), messageType, null);
            }
            Object(message, postPrintOption);
            Settings.ResetToDefaultColors();
        }

        public static void Message(string message, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(message);
            Object(message, postPrintOption);
        }

        public static void Exception(Exception exception, PostPrintOption postPrintOption = PostPrintOption.PrintNewLine)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(exception);

            (char postPrintChar, bool waitForKeyPress) = postPrintOption.Divide();

            Settings.ToggleCursorVisibility();

            Seperator();
            Object($"{exception}{postPrintChar}", PostPrintOption.None);
            Seperator();

            Settings.ToggleCursorVisibility();

            if (waitForKeyPress)
                ReadKey(true);
        }

        public static void Header(string text, int emptyLinesFollowing = 1, char borderChar = '=')
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(text);
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(text.Length, ComparisonType.LessThanOrEqualTo, 0, nameof(text), "Cannot be empty.");

            Settings.ToggleCursorVisibility();

            int borderLength = text.Length;
            Seperator(borderLength, borderChar);
            Settings.SetHeaderColors();
            Object(text, PostPrintOption.PrintNewLine);
            Settings.ResetToDefaultColors();
            Seperator(borderLength, borderChar);
            if (emptyLinesFollowing > 0)
                EmptyLines(emptyLinesFollowing);
            Settings.ToggleCursorVisibility();
        }

        public static void Seperator(char seperatorChar = '-')
        {
            Object(new string(seperatorChar, BufferWidth), PostPrintOption.PrintNewLine);
        }

        public static void Seperator(int length, char seperatorChar = '-')
        {
            Object(new string(seperatorChar, length), PostPrintOption.PrintNewLine);
        }

        public static void EmptyLines(int count)
        {
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(count, ComparisonType.LessThan, 1, nameof(count));

            Object(new string('\n', count), PostPrintOption.None);
        }

        public static void ClearLine(bool fromCurrentPosition = false)
        {
            int cursorLeft = fromCurrentPosition ? CursorLeft : 0;
            int count = BufferWidth - CursorLeft;
            StoreCurrentPosition();
            ClearFrom(CursorTop, cursorLeft, count);
            GoToStoredPosition();
        }

        public static void ClearLineAt(int cursorTop)
        {
            StoreCurrentPosition();
            ClearFrom(cursorTop, 0, BufferWidth);
            GoToStoredPosition();
        }

        public static void ClearLines(int count)
        {
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(count, ComparisonType.LessThan, 1, nameof(count));

            StoreCurrentPosition();
            ClearFrom(CursorTop, CursorLeft, count * BufferWidth);
            GoToStoredPosition();
        }

        public static void ClearLinesFrom(int cursorTop, int count)
        {
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(count, ComparisonType.LessThan, 1, nameof(count));

            StoreCurrentPosition();
            ClearFrom(cursorTop, CursorLeft, count * BufferWidth);
            GoToStoredPosition();
        }

        public static void ClearArea(int startTop, int startLeft, int endTop, int endLeft)
        {
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(startTop, ComparisonType.GreaterThan, endTop, nameof(startTop));
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(startLeft, ComparisonType.GreaterThan, endLeft, nameof(endLeft));

            StoreCurrentPosition();
            ClearFrom(startTop, startLeft, CalculateCharCount());
            GoToStoredPosition();

            int CalculateCharCount()
            {
                int rowCount = endTop - startTop + 1;
                int firstRowCharCount = BufferWidth - startLeft - 1;
                if (rowCount == 1)
                    return firstRowCharCount;
                int lastRowCharCount = BufferWidth - endLeft - 1;
                if (rowCount == 2)
                    return firstRowCharCount + lastRowCharCount;
                rowCount -= 2;
                return firstRowCharCount + lastRowCharCount + BufferWidth * rowCount;
            }
        }

        private static void ClearFrom(int cursorTop, int cursorLeft, int count)
        {
            Settings.ToggleCursorVisibility();

            Settings.SetCursorPosition(cursorTop, cursorLeft);
            Object(new string('\0', count), PostPrintOption.None);

            Settings.ToggleCursorVisibility();
        }

        private static void StoreCurrentPosition() => _storedPosition = (CursorTop, CursorLeft);

        private static void GoToStoredPosition() => Settings.SetCursorPosition(_storedPosition.Top, _storedPosition.Left);
    }

    public class PrintSettings
    {
        public ConsoleColor DefaultBackgroundColor { get; set; } = ConsoleColor.Black;
        public ConsoleColor DefaultForegroundColor { get; set; } = ConsoleColor.White;
        public ConsoleColor HeaderBackgroundColor { get; set; } = ConsoleColor.Green;
        public ConsoleColor HeaderForegroundColor { get; set; } = ConsoleColor.Black;
        public ConsoleColor SuccessBackgroundColor { get; set; } = ConsoleColor.Green;
        public ConsoleColor SuccessForegroundColor { get; set; } = ConsoleColor.Black;
        public ConsoleColor WarningBackgroundColor { get; set; } = ConsoleColor.Yellow;
        public ConsoleColor WarningForegroundColor { get; set; } = ConsoleColor.Black;
        public ConsoleColor ErrorBackgroundColor { get; set; } = ConsoleColor.Red;
        public ConsoleColor ErrorForegroundColor { get; set; } = ConsoleColor.Black;
        public bool HideCursorWhilePrinting { get; set; } = true;
        public string StringToPrintIfObjectIsNull { get; set; } = "<object is null>";
        public string StringToPrintIfEnuamerableIsNull { get; set; } = "<enumerable is null>";
        public string StringToPrintIfEnuamerableIsEmpty { get; set; } = "<enumerable is empty>";
        public string ProgramName { get; set; }

        public void SetHeaderColors()
        {
            SetColors(HeaderBackgroundColor, HeaderForegroundColor);
        }

        public void SetSuccessColors()
        {
            SetColors(SuccessBackgroundColor, SuccessForegroundColor);
        }

        public void SetWarningColors()
        {
            SetColors(WarningBackgroundColor, WarningForegroundColor);
        }

        public void SetErrorColors()
        {
            SetColors(ErrorBackgroundColor, ErrorForegroundColor);
        }

        public void ResetToDefaultColors()
        {
            SetColors(DefaultBackgroundColor, DefaultForegroundColor);
        }

        public void SwapColors()
        {
            SetColors(ForegroundColor, BackgroundColor);
        }

        public void SetColors(ConsoleColor background, ConsoleColor foreground)
        {
            BackgroundColor = background;
            ForegroundColor = foreground;
        }

        public void SetCursorPosition(int top, int left)
        {
            Console.SetCursorPosition(left, top);
        }

        public void ToggleCursorVisibility()
        {
            if (HideCursorWhilePrinting)
                CursorVisible = !CursorVisible;
        }
    }

    public enum PostPrintOption
    {
        None,
        PrintNewLine,
        WaitForKeyPress,
        PrintNewLineAndWaitForKeyPress
    }

    public enum ObjectSeperator
    {
        WhiteSpace,
        Tab,
        Comma,
        Dash,
        Pipe,
        NewLine
    }

    public enum MessageType
    {
        Basic,
        Success,
        Warning,
        Error
    }

    public static class PostPrintOptionExtensions
    {
        public static (char postPrintChar, bool waitForKeyPress) Divide(this PostPrintOption postPrintOption)
        {
            switch (postPrintOption)
            {
                case PostPrintOption.WaitForKeyPress:
                    return ('\0', true);
                case PostPrintOption.PrintNewLineAndWaitForKeyPress:
                    return ('\n', true);
                default:
                    return (postPrintOption == PostPrintOption.None ? '\0' : '\n', false);
            }
        }
    }

    public static class ObjectSeperatorExtensions
    {
        public static string ToCleanString(this ObjectSeperator objectSeperator)
        {
            switch (objectSeperator)
            {
                case ObjectSeperator.WhiteSpace:
                    return " ";
                case ObjectSeperator.Tab:
                    return "\t";
                case ObjectSeperator.Comma:
                    return ", ";
                case ObjectSeperator.Dash:
                    return " - ";
                case ObjectSeperator.Pipe:
                    return " | ";
                case ObjectSeperator.NewLine:
                    return "\n";
                default:
                    throw new ArgumentOutOfRangeException(nameof(objectSeperator), objectSeperator, null);
            }
        }
    }
}