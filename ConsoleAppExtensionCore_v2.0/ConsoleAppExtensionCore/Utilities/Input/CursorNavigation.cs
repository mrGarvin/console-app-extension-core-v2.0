﻿using ConsoleAppExtensionCore.Interfaces;
using ConsoleAppExtensionCore.Interfaces.Input;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public class CursorNavigation : ICursorNavigation
    {
        private ICursorInformation _information;
        private readonly IConsoleInformation _consoleInformation;

        private int CursorTop
        {
            get => _consoleInformation.CursorTop;
            set => _consoleInformation.CursorTop = value;
        }

        private int CursorLeft
        {
            get => _consoleInformation.CursorLeft;
            set
            {
                if (value < 0)
                {
                    _consoleInformation.CursorLeft = _consoleInformation.BufferWidth - 1;
                    _consoleInformation.CursorTop--;
                }
                else if (value >= _consoleInformation.BufferWidth)
                {
                    _consoleInformation.CursorLeft = 0;
                    _consoleInformation.CursorTop++;
                }
                else
                    _consoleInformation.CursorLeft = value;
            }
        }

        public CursorNavigation(ICursorInformation information, IConsoleInformation consoleInformation)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(information, consoleInformation);
            _information = information;
            _consoleInformation = consoleInformation;
        }

        public void MoveLeft()
        {
            if (_information.AtFirstColumn)
                return;
            CursorLeft--;
            if (CursorLeft > _information.LastColumnOfRow)
                CursorLeft = _information.LastColumnOfRow;
        }

        public void MoveRight()
        {
            if (_information.AtLastColumn)
                return;
            CursorLeft++;
            if (!_information.AtBottomRow && CursorLeft > _information.LastColumnOfRow)
            {
                CursorTop++;
                CursorLeft = 0;
            }
        }

        public void MoveUp()
        {
            if (_information.AtTopRow)
                return;
            CursorTop--;
            if (_information.AtTopRow && CursorLeft < _information.FirstColumn)
                CursorLeft = _information.FirstColumn;
            else if (CursorLeft > _information.LastColumnOfRow)
                CursorLeft = _information.LastColumnOfRow;
        }

        public void MoveDown()
        {
            if (_information.AtBottomRow)
                return;
            CursorTop++;
            if (_information.AtBottomRow && CursorLeft > _information.LastColumn)
                CursorLeft = _information.LastColumn;
            else if (CursorLeft > _information.LastColumnOfRow)
                CursorLeft = _information.LastColumnOfRow;
        }

        public void MoveToStartOfRow()
        {
            CursorLeft = _information.FirstColumnOfRow;
        }

        public void MoveToEndOfRow()
        {
            CursorLeft = _information.LastColumnOfRow;
        }

        public void MoveToTopRow()
        {
            if (_information.AtTopRow)
                return;
            CursorTop = _information.TopRow;
            if (CursorLeft < _information.FirstColumn)
                CursorLeft = _information.FirstColumn;
        }

        public void MoveToBottomRow()
        {
            if (_information.AtBottomRow)
                return;
            CursorTop = _information.BottomRow;
            if (CursorLeft > _information.LastColumn)
                CursorLeft = _information.LastColumn;
        }

        public ICursorNavigation Rest(ICursorInformation information)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(information);
            _information = information;
            return this;
        }
    }
}