﻿using System;
using System.Linq;
using ConsoleAppExtensionCore.Interfaces;
using ConsoleAppExtensionCore.Interfaces.Input;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public class CursorInformation : ICursorInformation
    {
        private IReadOnlyInputBuffer _buffer;
        private readonly IConsoleInformation _consoleInformation;

        public int TopRow { get; private set; }

        public int BottomRow
        {
            get
            {
                int bottomRow = TopRow;
                for (int rowIndex = _buffer.TopRowIndex; rowIndex < _buffer.RowCount; rowIndex++)
                    bottomRow += CalculateSubRowCount(_buffer[rowIndex].Count());
                return bottomRow - 1;
            }
        }
        public int FirstColumn { get; private set; }

        public int LastColumn
        {
            get
            {
                int charCountOfBottomRow = _buffer[_buffer.RowCount - 1].Count() - 1;
                int lastColumn = charCountOfBottomRow % _consoleInformation.BufferWidth;
                return lastColumn;
            }
        }
        public int FirstColumnOfRow => AtTopRow ? FirstColumn : 0;
        public int LastColumnOfRow
        {
            get
            {
                if (AtBottomRow)
                    return LastColumn;
                if (AtBottomSubRow())
                    return _buffer[_consoleInformation.CursorTop - TopRow].Count() - 1;
                return _consoleInformation.BufferWidth;
            }
        }

        public CursorInformation(IReadOnlyInputBuffer buffer, int topRow, IConsoleInformation consoleInformation)
        {
            ValidateInparameters(buffer, topRow);
            ConditionValidator.ThrowArgumentNullExceptionFor(consoleInformation);
            _buffer = buffer;
            TopRow = topRow;
            FirstColumn = _buffer.FirstColumnIndex;
            _consoleInformation = consoleInformation;
        }

        private void ValidateInparameters(IReadOnlyInputBuffer buffer, int topRow)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(buffer);
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(topRow, ComparisonType.LessThan, 0, nameof(topRow), "Cannot be less than 0.");
        }

        public bool AtTopRow => _consoleInformation.CursorTop == TopRow;

        public bool AtBottomRow => _consoleInformation.CursorTop == BottomRow;

        public bool AtFirstColumn => AtTopRow && _consoleInformation.CursorLeft == FirstColumn;

        public bool AtLastColumn => AtBottomRow && _consoleInformation.CursorLeft == LastColumn;

        public bool AtStartOfRow => _consoleInformation.CursorLeft == FirstColumnOfRow;

        public bool AtEndOfRow => _consoleInformation.CursorLeft == LastColumnOfRow;

        public ICursorInformation Reset(IReadOnlyInputBuffer buffer, int topRow)
        {
            ValidateInparameters(buffer, topRow);
            _buffer = buffer;
            TopRow = topRow;
            FirstColumn = _buffer.FirstColumnIndex;
            return this;
        }

        private bool AtBottomSubRow()
        {
            int minCursorTop = TopRow;
            int cursorTop = _consoleInformation.CursorTop;
            for (int bufferRowIndex = 0; bufferRowIndex < _buffer.RowCount; bufferRowIndex++)
            {
                int subRowCount = CalculateSubRowCount(_buffer[bufferRowIndex].Count());
                int maxCursorTop = minCursorTop + subRowCount;
                if (minCursorTop <= cursorTop && cursorTop < maxCursorTop)
                {
                    int subRowIndex = cursorTop - minCursorTop;
                    return subRowIndex == subRowCount - 1;
                }
                minCursorTop = maxCursorTop;
            }
            throw new OperationCanceledException("Could not calculate sub row index.");
        }

        private int CalculateSubRowCount(int charCount)
        {
            int numberOfSubRows = charCount / _consoleInformation.BufferWidth;
            if (numberOfSubRows == 0)
                return 1;
            if (charCount % _consoleInformation.BufferWidth == 0)
                return numberOfSubRows;
            return numberOfSubRows + 1;
        }
    }
}