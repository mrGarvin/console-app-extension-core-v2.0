﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleAppExtensionCore.Interfaces.Input;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public class InputBuffer : IInputBuffer
    {
        private readonly List<List<char>> _rows = new List<List<char>>();
        public IEnumerable<char> this[int rowIndex] => _rows[rowIndex];
        public char this[int rowIndex, int columnIndex] => _rows[rowIndex][columnIndex];
        public int TopRowIndex { get; private set; }
        public int FirstColumnIndex { get; private set; }
        public int RowCount => _rows.Count;

        public int InputCharCount
        {
            get
            {
                int charCount = _rows[TopRowIndex].Count - FirstColumnIndex;
                if ((TopRowIndex == 0 && RowCount == 1) || (TopRowIndex == 1 && RowCount == 2))
                    return charCount - 1;
                for (int rowIndex = TopRowIndex + 1; rowIndex < RowCount - 1; rowIndex++)
                    charCount += _rows[rowIndex].Count;
                int lastRowCharCount = _rows[RowCount - 1].Count;
                charCount += lastRowCharCount > 1 ? lastRowCharCount - 1 : lastRowCharCount;
                return charCount;
            }
        }

        public bool ContainsInput => InputCharCount > 0;

        public string Prompt { get; private set; }

        public InputBuffer(string prompt)
        {
            SetProperties(prompt);
        }

        private void SetProperties(string prompt)
        {
            ValidatePrompt();

            Prompt = prompt;
            _rows.Add(new List<char>(prompt));
            if (prompt.LastOrDefault() == '\n')
            {
                _rows.Add(new List<char> { '\n' });
                TopRowIndex = 1;
                FirstColumnIndex = 0;
            }
            else
            {
                _rows[0].Add('\n');
                TopRowIndex = 0;
                FirstColumnIndex = prompt.Length;
            }

            void ValidatePrompt()
            {
                ConditionValidator.ThrowArgumentNullExceptionFor(prompt);
                if (prompt.Contains("\0"))
                    throw new ArgumentException("Cannot contain null char.", nameof(prompt));
                if (prompt.Contains("\r"))
                    throw new ArgumentException("Cannot contain return char.", nameof(prompt));
                if (prompt.Contains("\t"))
                    throw new ArgumentException("Cannot contain tab char.", nameof(prompt));
                int newLineCharCount = prompt.Count(c => c == '\n');
                if (newLineCharCount > 1)
                    throw new ArgumentException("Can only contain one new line char.", nameof(prompt));
                if (newLineCharCount == 1 && prompt.Last() != '\n')
                    throw new ArgumentException("A new line char must be the last char.", nameof(prompt));
            }
        }

        public void Insert(int rowIndex, int columnIndex, char character)
        {
            ValidateCharacter();
            ValidateRowIndex();
            ValidateColumnIndex();

            if (character == '\n')
                InsertRow();
            else
                _rows[rowIndex].Insert(columnIndex, character);

            void ValidateCharacter()
            {
                switch (character)
                {
                    case '\0':
                        throw new InvalidOperationException("Cannot insert null char ('\\0').");
                    case '\r':
                        throw new InvalidOperationException("Cannot insert return char ('\\r').");
                    case '\t':
                        throw new InvalidOperationException("Cannot insert tab char ('\\t').");
                }
            }

            void ValidateRowIndex()
            {
                if (rowIndex < TopRowIndex)
                    throw new InvalidOperationException($"Cannot insert at index less than {nameof(TopRowIndex)}.");
            }

            void ValidateColumnIndex()
            {
                if (rowIndex == 0 && columnIndex < FirstColumnIndex)
                    throw new InvalidOperationException($"Cannot insert at index less than {nameof(FirstColumnIndex)}.");
                if (_rows[rowIndex].Count == 0 || columnIndex != _rows[rowIndex].Count)
                    return;
                if (_rows[rowIndex][_rows[rowIndex].Count - 1] == '\n')
                    throw new InvalidOperationException("Cannot insert a char after a new line char.");
            }

            void InsertRow()
            {
                if (columnIndex == _rows[rowIndex].Count - 1)
                    _rows.Insert(rowIndex + 1, new List<char>(new string('\n', 1)));
                else
                {
                    int count = _rows[rowIndex].Count - columnIndex;
                    List<char> trailingChars = _rows[rowIndex].GetRange(columnIndex, count);
                    _rows.Insert(rowIndex + 1, new List<char>(trailingChars));
                    _rows[rowIndex].RemoveRange(columnIndex, count - 1);
                }
            }
        }

        public void RemoveAt(int rowIndex, int columnIndex)
        {
            if (AtLastRow() && AtLastColumnOfRow())
                return;
            ValidateRowIndex();
            if (AtLastColumnOfRow())
                RemoveRow();
            else
            {
                ValidateColumnIndex();
                _rows[rowIndex].RemoveAt(columnIndex);
            }

            bool AtLastRow() => rowIndex == _rows.Count - 1;

            bool AtLastColumnOfRow() => columnIndex == _rows[rowIndex].Count - 1;

            void ValidateRowIndex()
            {
                if (rowIndex < TopRowIndex)
                    throw new InvalidOperationException($"Cannot remove at index less than {nameof(TopRowIndex)}.");
            }

            void ValidateColumnIndex()
            {
                if (rowIndex == 0 && columnIndex < FirstColumnIndex)
                    throw new InvalidOperationException($"Cannot remove at index less than {nameof(FirstColumnIndex)}.");
            }

            void RemoveRow()
            {
                int nextRowIndex = rowIndex + 1;
                List<char> nextRow = _rows[nextRowIndex];
                int count = nextRow.Count - 1;
                List<char> charsToMove = nextRow.GetRange(0, count);
                _rows[rowIndex].InsertRange(_rows[rowIndex].Count - 1, charsToMove);
                _rows.RemoveAt(nextRowIndex);
            }
        }

        public IInputBuffer Reset(string newPrompt)
        {
            _rows.Clear();
            SetProperties(newPrompt);
            return this;
        }

        public string InputToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int lastIndex = RowCount - 1;
            string rowAsString;
            int charCount;
            int columnIndex = FirstColumnIndex;
            for (int i = TopRowIndex; i < lastIndex; i++)
            {
                charCount = _rows[i].Count - columnIndex;
                rowAsString = _rows[i].EnumerableRangeToString(columnIndex, charCount);
                stringBuilder.Append(rowAsString);
                columnIndex = 0;
            }
            charCount = _rows[lastIndex].Count - columnIndex - 1;
            rowAsString = _rows[lastIndex].EnumerableRangeToString(columnIndex, charCount);
            stringBuilder.Append(rowAsString);
            return stringBuilder.ToString();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (List<char> row in _rows)
                stringBuilder.Append(row.EnumerableToString());
            return stringBuilder.ToString();
        }

        public IEnumerator<IEnumerable<char>> GetEnumerator()
        {
            return _rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}