﻿using ConsoleAppExtensionCore.Interfaces.Input;
using System;

namespace ConsoleAppExtensionCore.Utilities.Input.InputRange
{
    public abstract class BaseRange<T> : IRange
        where T : IComparable<T>
    {
        public T MinValue { get; }
        public T MaxValue { get; }
        public virtual string ErrorMessage { get; protected set; }
        
        protected BaseRange(T minValue, T maxValue)
        {
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(minValue, ComparisonType.GreaterThan, maxValue, nameof(minValue));
            MinValue = minValue;
            MaxValue = maxValue;
            ErrorMessage = $"Must be within the range {ToString()}.";
        }

        public virtual bool WithinRange(object obj)
        {
            T value = (T) obj;
            int compareToMinResult = value.CompareTo(MinValue);
            int compareToMaxResult = value.CompareTo(MaxValue);
            return 0 <= compareToMinResult && compareToMaxResult <= 0;
        }
        
        public virtual string MinValueToString() => MinValue.ToString();

        public virtual string MaxValueToString() => MaxValue.ToString();

        public override string ToString()
        {
            return $"{MinValue} to {MaxValue}";
        }
    }
}