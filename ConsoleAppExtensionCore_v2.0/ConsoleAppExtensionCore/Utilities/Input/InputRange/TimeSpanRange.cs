﻿using System;

namespace ConsoleAppExtensionCore.Utilities.Input.InputRange
{
    public class TimeSpanRange : BaseRange<TimeSpan>
    {
        public TimeSpanRange()
            : this(TimeSpan.MinValue)
        {
        }

        public TimeSpanRange(TimeSpan minValue)
            : this(minValue, TimeSpan.MaxValue)
        {
        }

        public TimeSpanRange(TimeSpan minValue, TimeSpan maxValue)
            : base(minValue, maxValue)
        {
        }

        public TimeSpanRange(string minValue, string maxValue)
            : base(TimeSpan.Parse(minValue), TimeSpan.Parse(maxValue))
        {
        }

        public override string MinValueToString() => MinValue.ToString("c");

        public override string MaxValueToString() => MaxValue.ToString("c");
    }
}