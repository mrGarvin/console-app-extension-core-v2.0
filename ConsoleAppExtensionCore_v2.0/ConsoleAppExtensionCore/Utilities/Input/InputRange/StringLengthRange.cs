﻿using System;

namespace ConsoleAppExtensionCore.Utilities.Input.InputRange
{
    public sealed class StringLengthRange : BaseRange<uint>
    {
        private const uint MaxStringLength = 1073741823;

        public static uint Max => MaxStringLength;

        public StringLengthRange()
            : this(0)
        {
        }
        
        public StringLengthRange(uint minLength)
            : this(minLength, MaxStringLength)
        {
        }

        public StringLengthRange(uint minLength, uint maxLength)
            : base(minLength, maxLength <= MaxStringLength ? maxLength : throw new InvalidOperationException($"{nameof(maxLength)} cannot be larger than {nameof(MaxStringLength)}."))
        {
            ErrorMessage = $"Must contain {ToString()} characters.";
        }

        public override bool WithinRange(object obj)
        {
            string value = (string) obj;
            return MinValue <= value.Length && value.Length <= MaxValue;
        }

        public override string ToString()
        {
            return $"{MinValue} to {MaxValue}";
        }
    }
}