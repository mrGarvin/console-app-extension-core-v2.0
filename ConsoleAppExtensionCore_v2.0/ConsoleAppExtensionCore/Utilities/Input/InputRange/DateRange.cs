﻿using System;

namespace ConsoleAppExtensionCore.Utilities.Input.InputRange
{
    public sealed class DateRange : BaseRange<DateTime>
    {
        public DateRange()
            : this(DateTime.MinValue)
        {
        }

        public DateRange(DateTime minValue)
            : this(minValue, DateTime.MaxValue)
        {
        }

        public DateRange(DateTime minValue, DateTime maxValue)
            : base(minValue, maxValue)
        {
        }

        public DateRange(string minValue, string maxValue)
            : base(DateTime.Parse(minValue).Date, DateTime.Parse(maxValue).Date)
        {
        }

        public override string MinValueToString() => MinValue.ToString("yyyy-MM-dd");

        public override string MaxValueToString() => MaxValue.ToString("yyyy-MM-dd");
    }
}