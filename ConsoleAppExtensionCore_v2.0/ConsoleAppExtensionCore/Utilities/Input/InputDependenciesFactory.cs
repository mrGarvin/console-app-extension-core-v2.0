﻿using ConsoleAppExtensionCore.Interfaces;
using ConsoleAppExtensionCore.Interfaces.Input;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public class InputDependenciesFactory
    {
        private readonly IConsoleInformation _consoleInformation;

        public InputDependenciesFactory(IConsoleInformation consoleInformation)
        {
            _consoleInformation = consoleInformation;
        }

        public IInputBuffer CreateInputBuffer(string prompt) => new InputBuffer(prompt);

        public ICursorInformation CreateCursorInformation(IReadOnlyInputBuffer buffer, int startCursorTop) => new CursorInformation(buffer, startCursorTop, _consoleInformation);

        public ICursorNavigation CreateCursorNavigation(ICursorInformation information) => new CursorNavigation(information, _consoleInformation);

        public IInputPositionCalculator CreateInputPositionCalculator(IReadOnlyInputBuffer buffer, int startCursorTop) => new InputPositionCalculator(buffer, startCursorTop, _consoleInformation);
    }
}