﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ConsoleAppExtensionCore.Interfaces.Input;
using ConsoleAppExtensionCore.Utilities.Input.InputRange;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public static class InputService
    {
        private static IInputBuffer _buffer;
        private static ICursorInformation _information;
        private static ICursorNavigation _navigation;
        private static IInputPositionCalculator _positionCalculator;
        private static IRange _range;
        private static InsertValidator _inputValidator;
        private static Type _returnType;

        private static int _storedRow;
        private static int _storedColumn;

        private static (int bufferRowIndex, int bufferColumnIndex) CurrentPositionInBuffer() => _positionCalculator
            .CalculatePositionInInputBuffer(Console.CursorTop, Console.CursorLeft);

        private static readonly IReadOnlyDictionary<Type, Type> TypeRangeMapper = new Dictionary<Type, Type>
        {
            { typeof(DateTime), typeof(DateRange) },
            { typeof(decimal), typeof(DecimalRange) },
            { typeof(double), typeof(DoubleRange) },
            { typeof(string), typeof(StringLengthRange) },
            { typeof(int), typeof(IntRange) },
            { typeof(TimeSpan), typeof(TimeSpanRange) },
            { typeof(uint), typeof(UIntRange) }
        };

        private static readonly IReadOnlyDictionary<Type, Type> TypeValidatorMapper = new Dictionary<Type, Type>
        {
            {typeof(DateTime), typeof(DateValidator)},
            {typeof(decimal), typeof(DecimalValidator)},
            {typeof(double), typeof(DoubleValidator)},
            {typeof(int), typeof(IntValidator)},
            {typeof(string), typeof(StringValidator)},
            {typeof(TimeSpan), typeof(TimeSpanValidator)},
            {typeof(uint), typeof(UIntValidator)}
        };

        public static bool PrintNewLineBeforeReturn { get; set; } = true;
        public static bool ClearConsoleBeforeReturn { get; set; }

        static InputService()
        {
            InputDependenciesFactory inputDependenciesFactory = new InputDependenciesFactory(new ConsoleInformation());
            _buffer = inputDependenciesFactory.CreateInputBuffer("");
            _information = inputDependenciesFactory.CreateCursorInformation(_buffer, 0);
            _navigation = inputDependenciesFactory.CreateCursorNavigation(_information);
            _positionCalculator = inputDependenciesFactory.CreateInputPositionCalculator(_buffer, 0);
        }

        private static void SetupFor<T>(string prompt, IRange range)
            where T : IComparable<T>
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(prompt);
            _returnType = typeof(T);
            _range = range ?? CreateRange<T>();
            _inputValidator = CreateInputValidator<T>();
            _buffer = _buffer.Reset(prompt);
            _information = _information.Reset(_buffer, Console.CursorTop);
            _navigation = _navigation.Rest(_information);
            _positionCalculator = _positionCalculator.Reset(_buffer, Console.CursorTop);
            Console.Write(prompt);
        }

        private static IRange CreateRange<T>()
            where T : IComparable<T>
        {
            return (BaseRange<T>) Activator.CreateInstance(TypeRangeMapper[typeof(T)]);
        }

        private static InsertValidator CreateInputValidator<T>()
            where T : IComparable<T>
        {
            return (InsertValidator) Activator.CreateInstance(TypeValidatorMapper[typeof(T)]);
        }

        public static bool GetBool(string prompt, bool trueAsDefault = true) => GetNullableBool(prompt, trueAsDefault, false).Value;

        public static bool? GetNullableBool(string prompt, bool trueAsDefault = true) => GetNullableBool(prompt, trueAsDefault, true);

        private static bool? GetNullableBool(string prompt, bool trueAsDefault, bool allowNull)
        {
            Console.Write(prompt);
            while (true)
            {
                ConsoleKeyInfo input = Console.ReadKey(true);
                switch (input.Key)
                {
                    case ConsoleKey.Escape:
                        if (allowNull)
                            return Return(null);
                        break;
                    case ConsoleKey.Enter:
                        return Return(trueAsDefault);
                    case ConsoleKey.Y:
                        return Return(true);
                    case ConsoleKey.N:
                        return Return(false);
                }
            }

            bool? Return(bool? output)
            {
                if (output.HasValue)
                {
                    char answerChar;
                    if (output.Value)
                        answerChar = trueAsDefault ? 'Y' : 'y';
                    else
                        answerChar = trueAsDefault ? 'n' : 'N';
                    Console.Write(answerChar);
                }
                return TearDown(output);
            }
        }
        
        public static DateTime GetDate(string prompt, DateRange range = null) => GetStructWithFormat(prompt, range, false).Value;

        public static DateTime? GetNullableDate(string prompt, DateRange range = null) => GetStructWithFormat(prompt, range, true);

        public static TimeSpan GetTimeSpan(string prompt, TimeSpanRange range = null) => GetStructWithFormat(prompt, range, false).Value;

        public static TimeSpan? GetNullableTimeSpan(string prompt, TimeSpanRange range = null) => GetStructWithFormat(prompt, range, true);

        private static T? GetStructWithFormat<T>(string prompt, BaseRange<T> range, bool allowNull)
            where T : struct, IComparable<T>
        {
            SetupFor<T>(prompt, range);
            InsertFormatInBuffer();
            return GetInput<T?>(false, allowNull);

            void InsertFormatInBuffer()
            {
                string format = _range.MinValueToString();
                (int bufferRowIndex, int bufferColumnIndex) position = CurrentPositionInBuffer();
                for (int index = 0; index < format.Length; index++)
                    _buffer.Insert(position.bufferRowIndex, position.bufferColumnIndex + index, format[index]);
                ReprintAll();
            }
        }

        public static decimal GetDecimal(string prompt, DecimalRange range = null) => GetNumber(prompt, range, false).Value;

        public static decimal? GetNullableDecimal(string prompt, DecimalRange range = null) => GetNumber(prompt, range, true);

        public static double GetDouble(string prompt, DoubleRange range = null) => GetNumber(prompt, range, false).Value;

        public static double? GetNullableDouble(string prompt, DoubleRange range = null) => GetNumber(prompt, range, true);

        public static int GetInt(string prompt, IntRange range = null) => GetNumber(prompt, range, false).Value;

        public static int? GetNullableInt(string prompt, IntRange range = null) => GetNumber(prompt, range, true);

        public static uint GetUInt(string prompt, UIntRange range = null) => GetNumber(prompt, range, false).Value;

        public static uint? GetNullableUInt(string prompt, UIntRange range = null) => GetNumber(prompt, range, true);

        public static string GetString(string prompt, StringLengthRange range = null, bool allowNewLine = true, bool allowNull = true)
        {
            SetupFor<string>(prompt, range);
            return GetInput<string>(allowNewLine, allowNull);
        }

        private static T? GetNumber<T>(string prompt, BaseRange<T> range, bool allowNull)
            where T : struct, IComparable<T>
        {
            SetupFor<T>(prompt, range);

            return GetInput<T?>(false, allowNull);
        }

        private static T GetInput<T>(bool allowNewLine, bool allowNull)
        {
            while (true)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        if (allowNull)
                            return TearDown(default(T));
                        break;
                    case ConsoleKey.Enter:
                        if (allowNewLine && keyInfo.Modifiers == ConsoleModifiers.Shift)
                        {
                            InsertNewLine();
                            break;
                        }
                        if (InputIsValid(out object validInput))
                            return TearDown((T)validInput);
                        break;
                    default:
                        if (IsRemoveKey(keyInfo.Key))
                            continue;
                        if (IsMoveKey(keyInfo.Key))
                            continue;
                        if (_inputValidator.CanInsert(keyInfo.KeyChar))
                            InsertChar(keyInfo.KeyChar);
                        break;
                }
            }
        }

        private static bool TryParseInput(out object result)
        {
            string input = _buffer.InputToString();
            try
            {
                MethodInfo tryParseMethod = _returnType.GetMethod("TryParse",
                    new[] { typeof(string), _returnType.MakeByRefType() });
                if (tryParseMethod is null)
                {
                    result = Convert.ChangeType(input, _returnType);
                    return true;
                }
                object[] parameters = new object[] { input, null };
                bool parseWasSuccessful = (bool)tryParseMethod.Invoke(null, parameters);
                result = parseWasSuccessful ? parameters[1] : null;
                return parseWasSuccessful;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        private static bool IsMoveKey(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    _navigation.MoveLeft();
                    break;
                case ConsoleKey.RightArrow:
                    _navigation.MoveRight();
                    break;
                case ConsoleKey.UpArrow:
                    _navigation.MoveUp();
                    break;
                case ConsoleKey.DownArrow:
                    _navigation.MoveDown();
                    break;
                case ConsoleKey.Home:
                    _navigation.MoveToStartOfRow();
                    break;
                case ConsoleKey.End:
                    _navigation.MoveToEndOfRow();
                    break;
                case ConsoleKey.PageUp:
                    _navigation.MoveToTopRow();
                    break;
                case ConsoleKey.PageDown:
                    _navigation.MoveToBottomRow();
                    break;
                default:
                    return false;
            }
            return true;
        }

        private static bool IsRemoveKey(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.Backspace:
                    if (_information.AtFirstColumn)
                        return true;
                    _navigation.MoveLeft();
                    break;
                case ConsoleKey.Delete:
                    if (_information.AtLastColumn)
                        return true;
                    break;
                default:
                    return false;
            }
            if (!_buffer.ContainsInput || !_inputValidator.CanRemove())
                return true;
            (int bufferRowIndex, int bufferColumnIndex) position = CurrentPositionInBuffer();
            _buffer.RemoveAt(position.bufferRowIndex, position.bufferColumnIndex);
            ReprintAll();
            return true;
        }

        private static void InsertNewLine()
        {
            (int bufferRowIndex, int bufferColumnIndex) position = CurrentPositionInBuffer();
            _buffer.Insert(position.bufferRowIndex, position.bufferColumnIndex, '\n');
            Console.CursorVisible = false;
            Console.CursorTop++;
            Console.CursorLeft = 0;
            ReprintAll();
        }

        private static void InsertChar(char character)
        {
            (int bufferRowIndex, int bufferColumnIndex) position = CurrentPositionInBuffer();
            _buffer.Insert(position.bufferRowIndex, position.bufferColumnIndex, character);
            _navigation.MoveRight();
            ReprintAll();
        }

        private static void ReprintAll()
        {
            Console.CursorVisible = false;
            StoreCurrentPosition();

            ClearAllLines();
            Console.Write(_buffer.ToString());

            if (_buffer.ContainsInput)
                ValidateInput();

            GoToStoredPosition();
            Console.CursorVisible = true;
        }

        private static void ValidateInput() => InputIsValid(out object validInput);

        private static bool InputIsValid(out object validInput)
        {
            if (!TryParseInput(out validInput))
                return TearDown($"Can not parse to {_returnType.Name}.");
            if (!_range.WithinRange(validInput))
            {
                validInput = null;
                return TearDown(_range.ErrorMessage);
            }
            return true;

            bool TearDown(string errorMessage)
            {
                PrintError(errorMessage);
                return false;
            }
        }

        private static void ClearAllLines()
        {
            SetCursorPosition(_information.TopRow, 0);

            int rowCount = _information.BottomRow - Console.CursorTop + 3;
            Console.Write(new string('\0', Console.BufferWidth * rowCount));

            SetCursorPosition(_information.TopRow, 0);
        }

        private static void PrintError(string message)
        {
            int top = Console.CursorTop,
                left = Console.CursorLeft;
            SetCursorPosition(_information.BottomRow + 1, 0);
            Print.Error(message, PostPrintOption.None);
            SetCursorPosition(top, left);
        }

        private static void ClearErrorMessage()
        {
            Console.CursorVisible = false;
            StoreCurrentPosition();

            SetCursorPosition(_information.BottomRow + 1, 0);
            Console.Write(new string('\0', Console.BufferWidth));

            GoToStoredPosition();
            Console.CursorVisible = true;
        }

        private static void StoreCurrentPosition()
        {
            _storedRow = Console.CursorTop;
            _storedColumn = Console.CursorLeft;
        }

        private static void GoToStoredPosition()
        {
            Console.CursorTop = _storedRow;
            Console.CursorLeft = _storedColumn;
        }

        private static void SetCursorPosition(int top, int left)
        {
            Console.SetCursorPosition(left, top);
        }

        private static T TearDown<T>(T value)
        {
            if (typeof(T) != typeof(bool))
                ClearErrorMessage();
            if (PrintNewLineBeforeReturn)
                Console.WriteLine();
            if (ClearConsoleBeforeReturn)
                Console.Clear();
            return value;
        }

        private abstract class InsertValidator
        {
            public abstract bool CanInsert(char character);
            public virtual bool CanRemove() => true;
        }

        private abstract class FloatValidator<T> : InsertValidator
            where T : IComparable<T>
        {
            private readonly bool _minValueIsPositive;

            private char FirstCharacter => _buffer[_buffer.TopRowIndex, _buffer.FirstColumnIndex];

            protected FloatValidator(bool minValueIsPositive)
            {
                _minValueIsPositive = minValueIsPositive;
            }

            public override bool CanInsert(char character)
            {
                if (!IsValidChar(character))
                    return false;
                if (character == '-' && !CanInsertMinusChar())
                    return false;
                if (character == '.' && !CanInsertPeriod())
                    return false;
                return true;
            }

            private bool IsValidChar(char character) => character == '-' || character == '.' || char.IsDigit(character);

            private bool CanInsertMinusChar()
            {
                if (_minValueIsPositive)
                    return false;
                if (!_information.AtFirstColumn)
                    return false;
                if (FirstCharacter == '-')
                    return false;
                return true;
            }

            private bool CanInsertPeriod() => !_buffer.InputToString().Contains('.');
        }

        private abstract class FormatValidator : InsertValidator
        {
            protected char SeperatorCharacter { get; }

            protected FormatValidator(char seperatorCharacter)
            {
                SeperatorCharacter = seperatorCharacter;
            }

            protected int GetCurrentIndexIn(string input)
            {
                (int bufferRowIndex, int bufferColumnIndex) position = CurrentPositionInBuffer();
                int columnIndex = position.bufferColumnIndex - _information.FirstColumn;
                if (position.bufferRowIndex == 0)
                    return columnIndex;
                string[] buffer = input.Split('\n');
                for (int rowIndex = 1; rowIndex < buffer.Length; rowIndex++)
                {
                    columnIndex += buffer[rowIndex].Length;
                    if (rowIndex == position.bufferRowIndex)
                        return columnIndex;
                }
                throw new OperationCanceledException($"Could not calculate the index in {input}.");
            }

            protected bool InFirstSection(string input, int index, out string firstSection)
            {
                int lastIndexInSection = input.IndexOf(SeperatorCharacter);
                firstSection = null;
                if (index > lastIndexInSection)
                    return false;
                firstSection = input.Substring(0, lastIndexInSection);
                return true;
            }

            protected bool InSecondSection(string input, int index, out string secondSection)
            {
                int firstIndexInSection = input.IndexOf(SeperatorCharacter);
                int lastIndexInSection = input.LastIndexOf(SeperatorCharacter);
                secondSection = null;
                if (firstIndexInSection > index || index > lastIndexInSection)
                    return false;
                secondSection = input.Remove(lastIndexInSection);
                secondSection = secondSection.Remove(0, firstIndexInSection + 1);
                return true;
            }

            protected bool InThirdSection(string input, int index, out string thirdSection)
            {
                int firstIndexInSection = input.LastIndexOf(SeperatorCharacter);
                thirdSection = null;
                if (index < firstIndexInSection)
                    return false;
                thirdSection = input.Substring(firstIndexInSection).Remove(0, 1);
                return true;
            }
        }

        private class DateValidator : FormatValidator
        {
            public DateValidator()
                : base('-')
            {
            }

            public override bool CanInsert(char character) => char.IsDigit(character);

            public override bool CanRemove()
            {
                string input = _buffer.InputToString();
                int index = GetCurrentIndexIn(input);
                if (input[index] == SeperatorCharacter)
                    return false;
                if (InFirstSection(input, index, out string firstSection) && firstSection.Length > 1)
                    return true;
                if (InSecondSection(input, index, out string secondSection) && secondSection.Length > 1)
                    return true;
                if (InThirdSection(input, index, out string thirdSection) && thirdSection.Length > 1)
                    return true;
                return false;
            }
        }

        private class DecimalValidator : FloatValidator<decimal>
        {
            public DecimalValidator()
                : base(((DecimalRange)_range).MinValue > -1)
            {
            }
        }

        private class DoubleValidator : FloatValidator<double>
        {
            public DoubleValidator()
                : base(((DoubleRange) _range).MinValue > -1)
            {
            }
        }

        private class IntValidator : InsertValidator
        {
            private readonly IntRange _intRange;

            public IntValidator()
            {
                _intRange = (IntRange)_range;
            }

            private char FirstCharacter => _buffer[_buffer.TopRowIndex, _buffer.FirstColumnIndex];

            public override bool CanInsert(char character)
            {
                if (!IsValidChar(character))
                    return false;
                if (character == '-' && !CanInsertMinusChar())
                    return false;
                return true;
            }

            private bool IsValidChar(char character) => character == '-' || char.IsDigit(character);

            private bool CanInsertMinusChar()
            {
                if (_intRange.MinValue > -1)
                    return false;
                if (!_information.AtFirstColumn)
                    return false;
                if (FirstCharacter == '-')
                    return false;
                return true;
            }
        }

        private class StringValidator : InsertValidator
        {
            public override bool CanInsert(char character)
            {
                return char.IsLetterOrDigit(character) || char.IsSymbol(character) ||
                       char.IsPunctuation(character) || char.IsSeparator(character);
            }
        }

        private class TimeSpanValidator : FormatValidator
        {
            private readonly TimeSpanRange _timeSpanRange;

            public TimeSpanValidator()
                : base(':')
            {
                _timeSpanRange = (TimeSpanRange)_range;
            }

            private char FirstCharacter => _buffer[_buffer.TopRowIndex, _buffer.FirstColumnIndex];

            public override bool CanInsert(char character)
            {
                if (!IsValidChar(character))
                    return false;
                if (character == '-' && !CanInsertMinusChar())
                    return false;
                if (character == '.' && !CanInsertPeriod())
                    return false;
                if (!CanInsertDigit())
                    return false;
                return true;
            }

            public override bool CanRemove()
            {
                string input = _buffer.InputToString();
                int index = GetCurrentIndexIn(input);
                if (input[index] == SeperatorCharacter)
                    return false;
                if (InFirstSection(input, index, out string firstSection) && firstSection.Length > 1)
                    return true;
                if (InSecondSection(input, index, out string secondSection) && secondSection.Length > 1)
                    return true;
                if (InThirdSection(input, index, out string thirdSection) && thirdSection.Length > 1)
                    return true;
                return false;
            }

            private bool IsValidChar(char character) => character == '-' || character == '.' || char.IsDigit(character);

            private bool CanInsertMinusChar()
            {
                if (!_information.AtFirstColumn)
                    return false;
                if (_timeSpanRange.MinValue >= TimeSpan.Zero)
                    return false;
                if (FirstCharacter == '-')
                    return false;
                return true;
            }

            private bool CanInsertPeriod()
            {
                string input = _buffer.InputToString();
                int index = GetCurrentIndexIn(input);
                if (InFirstSection(input, index, out string firstSection) && !firstSection.Contains('.'))
                    return true;
                if (InThirdSection(input, index, out string thirdSection) && !thirdSection.Contains('.'))
                    return true;
                return false;
            }

            private bool CanInsertDigit()
            {
                if (_information.AtFirstColumn && _buffer[_buffer.TopRowIndex, _buffer.FirstColumnIndex] == '-')
                    return false;
                return true;
            }
        }

        private class UIntValidator : InsertValidator
        {
            public override bool CanInsert(char character) => char.IsDigit(character);
        }
    }
}