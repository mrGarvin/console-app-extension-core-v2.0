﻿using System;
using System.Linq;
using ConsoleAppExtensionCore.Interfaces;
using ConsoleAppExtensionCore.Interfaces.Input;

namespace ConsoleAppExtensionCore.Utilities.Input
{
    public class InputPositionCalculator : IInputPositionCalculator
    {
        private IReadOnlyInputBuffer _buffer;
        private readonly IConsoleInformation _consoleInformation;
        private int _topRow;

        public InputPositionCalculator(IReadOnlyInputBuffer buffer, int topRow, IConsoleInformation consoleInformation)
        {
            ValidateInparameters(buffer, topRow);
            ConditionValidator.ThrowArgumentNullExceptionFor(consoleInformation);
            _buffer = buffer;
            _consoleInformation = consoleInformation;
            _topRow = topRow;
        }

        private void ValidateInparameters(IReadOnlyInputBuffer buffer, int startCursorTop)
        {
            ConditionValidator.ThrowArgumentNullExceptionFor(buffer);
            ConditionValidator.ThrowArgumentOutOfRangeExceptionIf(startCursorTop, ComparisonType.LessThan, 0, nameof(startCursorTop), "Cannot be less than 0.");
        }

        public (int bufferRowIndex, int bufferColumnIndex) CalculatePositionInInputBuffer(int cursorTop, int cursorLeft)
        {
            (int bufferRowIndex, int subRowIndex) result = CalculateBufferRowIndexAndSubRowIndex(cursorTop);
            int columnIndex = cursorLeft + result.subRowIndex * _consoleInformation.BufferWidth;
            return (result.bufferRowIndex, columnIndex);
        }

        public (int cursorTop, int cursorLeft) CalculatePositionInConsole(int bufferRowIndex, int bufferColumnIndex)
        {
            int minCursorTop = _topRow;
            int cursorTop = _consoleInformation.CursorTop;
            for (int index = bufferRowIndex; index < _buffer.RowCount; index++)
            {
                int subRowCount = CalculateSubRowCount(_buffer[index].Count());
                int maxCursorTop = minCursorTop + subRowCount;
                if (minCursorTop < cursorTop && cursorTop < maxCursorTop)
                {
                    cursorTop -= minCursorTop;
                    int cursorLeft = bufferColumnIndex % _consoleInformation.BufferWidth;
                    return (cursorTop, cursorLeft);
                }
                minCursorTop = maxCursorTop;
            }
            throw new OperationCanceledException("Could not calculate position in Console.");
        }

        public IInputPositionCalculator Reset(IReadOnlyInputBuffer buffer, int startCursorTop)
        {
            ValidateInparameters(buffer, startCursorTop);
            _buffer = buffer;
            _topRow = startCursorTop;
            return this;
        }

        private (int bufferRowIndex, int subRowIndex) CalculateBufferRowIndexAndSubRowIndex(int cursorTop)
        {
            int minCursorTop = _topRow;
            for (int bufferRowIndex = 0; bufferRowIndex < _buffer.RowCount; bufferRowIndex++)
            {
                int subRowCount = CalculateSubRowCount(_buffer[bufferRowIndex].Count());
                int maxCursorTop = minCursorTop + subRowCount;
                if (minCursorTop <= cursorTop && cursorTop < maxCursorTop)
                {
                    int subRowIndex = cursorTop - minCursorTop;
                    return (bufferRowIndex, subRowIndex);
                }
                minCursorTop = maxCursorTop;
            }
            throw new OperationCanceledException("Could not calculate buffer row index and sub row index.");
        }

        private int CalculateSubRowCount(int charCount)
        {
            int numberOfSubRows = charCount / _consoleInformation.BufferWidth;
            if (numberOfSubRows == 0)
                return 1;
            if (charCount % _consoleInformation.BufferWidth == 0)
                return numberOfSubRows;
            return numberOfSubRows + 1;
        }
    }
}