﻿using System;
using System.Collections.Generic;
using ConsoleAppExtensionCore;
using ConsoleAppExtensionCore.Interfaces.Menu;
using ConsoleAppExtensionCore.Utilities;
using ConsoleAppExtensionCore.Utilities.Input;
using ConsoleAppExtensionCore.Utilities.Input.InputRange;
using ConsoleAppExtensionCore.Utilities.Menu;

namespace TestConsole
{
    public class Program : ConsoleExtensionCore
    {
        private static StringLengthRange StringLengthRange { get; set; } = new StringLengthRange();
        private static bool AllowNewLine { get; set; } = true;
        private static bool AllowNull { get; set; } = true;

        private static DateRange DateRange { get; set; } = new DateRange();
        private static TimeSpanRange TimeSpanRange { get; set; } = new TimeSpanRange();
        private static IntRange IntRange { get; set; } = new IntRange();
        private static UIntRange UIntRange { get; set; } = new UIntRange();
        private static DoubleRange DoubleRange { get; set; } = new DoubleRange();
        private static DecimalRange DecimalRange { get; set; } = new DecimalRange();

        public static void Main(string[] args)
        {
            new Program();
        }

        protected override void Initialize()
        {
            AddOptions(CreateSubMenuOption(MenuOptionKey.One, CreateGetInputMenu()));
        }

        private IMenu CreateGetInputMenu()
        {
            IMenu inputMenu = CreateNewSubMenu(
                "Get input",
                CreateSubMenuOption(MenuOptionKey.Zero, CreateInputSettingsMenu()),
                CreateSubMenuOption(MenuOptionKey.One, CreateGetValueMenu<DateTime>()),
                CreateSubMenuOption(MenuOptionKey.Two, CreateGetValueMenu<decimal>()),
                CreateSubMenuOption(MenuOptionKey.Three, CreateGetValueMenu<double>()),
                CreateSubMenuOption(MenuOptionKey.Four, CreateGetValueMenu<int>()),
                CreateSubMenuOption(MenuOptionKey.Five, CreateGetStringMenu()),
                CreateSubMenuOption(MenuOptionKey.Six, CreateGetValueMenu<TimeSpan>()),
                CreateSubMenuOption(MenuOptionKey.Seven, CreateGetValueMenu<uint>())
            );
            return inputMenu;
        }

        private IMenu CreateInputSettingsMenu()
        {
            IMenu printNewLineBeforeReturnMenu = CreateYesOrNoMenu(
                "Print new line before return?",
                () => InputService.PrintNewLineBeforeReturn = true,
                () => InputService.PrintNewLineBeforeReturn = false
            );
            IMenu clearConsoleBeforeReturnMenu = CreateYesOrNoMenu(
                "Clear console before return?",
                () => InputService.ClearConsoleBeforeReturn = true,
                () => InputService.ClearConsoleBeforeReturn = false
            );
            IMenu inputSettingsMenu = CreateNewSubMenu(
                "InputService settings",
                CreateSubMenuOption(MenuOptionKey.One, printNewLineBeforeReturnMenu),
                CreateSubMenuOption(MenuOptionKey.Two, clearConsoleBeforeReturnMenu)
            );
            return inputSettingsMenu;
        }

        private IMenu CreateGetValueMenu<TValue>()
            where TValue : struct, IComparable<TValue>
        {
            string valueTypeName = Mapper.GetNameOf<TValue>().ToLower();
            IMenu getValueMenu = CreateNewSubMenu(
                $"Get {valueTypeName}",
                CreateMenuFuncOption(MenuOptionKey.R, $"Set {valueTypeName} range", SetRange<TValue>),
                CreateMenuFuncOption(MenuOptionKey.V, $"Get {valueTypeName}", GetValue<TValue>),
                CreateMenuFuncOption(MenuOptionKey.N, $"Get nullable {valueTypeName}", GetNullableValue<TValue>)
            );
            return getValueMenu;
        }

        private PostInvokeCommand GetValue<TValue>()
        {
            do
            {
                string prompt = $"{Mapper.GetNameOf<TValue>()}: ";
                object result = Mapper.GetValueFuncFor<TValue>()(prompt);
                Print.Object(result, PostPrintOption.PrintNewLine);
            } while (Confirm($"Do you want to enter an other {Mapper.GetNameOf<TValue>().ToLower()}?"));
            return PostInvokeCommand.StayInSubmenu;
        }

        private PostInvokeCommand GetNullableValue<TValue>()
            where TValue : struct
        {
            do
            {
                string prompt = $"Nullable {Mapper.GetNameOf<TValue>().ToLower()}: ";
                object result = Mapper.GetNullableValueFuncFor<TValue>()(prompt);
                Print.Object(result, PostPrintOption.PrintNewLine);
            } while (Confirm($"Do you want to enter an other nullable {Mapper.GetNameOf<TValue>().ToLower()}?"));
            return PostInvokeCommand.StayInSubmenu;
        }

        private PostInvokeCommand SetRange<TValue>()
            where TValue : struct
        {
            Print.Header($"Set {Mapper.GetNameOf<TValue>().ToLower()} range");
            object min = Mapper.GetNullableValueFuncFor<TValue>()("Min: ");
            if (min == null)
            {
                min = Mapper.GetDefaultMinValueOf<TValue>();
                Print.Message($"Min set to: {min}", PostPrintOption.PrintNewLine);
            }
            object max = Mapper.GetNullableValueFuncFor<TValue>()("Max: ");
            if (max == null)
            {
                max = Mapper.GetDefaultMaxValueOf<TValue>();
                Print.Message($"Max set to: {max}", PostPrintOption.PrintNewLineAndWaitForKeyPress);
            }
            else
                Console.ReadKey();
            Mapper.GetSetRangeActionFor<TValue>()(CreateRangeFor<TValue>((TValue) min, (TValue)max));
            return PostInvokeCommand.StayInSubmenu;
        }
        
        private object CreateRangeFor<TValue>(TValue? minValue = null, TValue? maxValue = null)
            where TValue : struct
        {
            if (minValue is null)
                return Activator.CreateInstance(Mapper.GetRangeTypeFor<TValue>());
            if (maxValue is null)
                return Activator.CreateInstance(Mapper.GetRangeTypeFor<TValue>(), minValue);
            return Activator.CreateInstance(Mapper.GetRangeTypeFor<TValue>(), minValue, maxValue);
        }

        private IMenu CreateGetStringMenu()
        {
            IMenu allowNewLineMenu = CreateYesOrNoMenu(
                "Allow new line?",
                () => AllowNewLine = true,
                () => AllowNewLine = false
            );
            IMenu allowNullMenu = CreateYesOrNoMenu(
                "Allow null?",
                () => AllowNull = true,
                () => AllowNull = false
            );
            IMenu settingsMenu = CreateNewSubMenu(
                "Settings",
                CreateSubMenuOption(MenuOptionKey.One, allowNewLineMenu),
                CreateSubMenuOption(MenuOptionKey.Two, allowNullMenu),
                CreateMenuFuncOption(MenuOptionKey.Three, "Set string length range", SetStringLengthRange)
            );

            IMenu getStringMenu = CreateNewSubMenu(
                "Get string",
                CreateSubMenuOption(MenuOptionKey.S, settingsMenu),
                CreateMenuFuncOption(MenuOptionKey.G, "Get string", GetValue<string>)
            );
            return getStringMenu;

            PostInvokeCommand SetStringLengthRange()
            {
                Print.Header("Set string length range");
                uint? min = InputService.GetNullableUInt("Min length: ", new UIntRange(0, StringLengthRange.Max));
                if (min is null)
                {
                    min = uint.MinValue;
                    Print.Message($"Min set to: {min}", PostPrintOption.PrintNewLine);
                }
                uint? max = InputService.GetNullableUInt("Max length: ", new UIntRange(min.Value == 0 ? 1 : min.Value, StringLengthRange.Max));
                if (max is null)
                {
                    max = StringLengthRange.Max;
                    Print.Message($"Max set to: {max}", PostPrintOption.PrintNewLineAndWaitForKeyPress);
                }
                else
                    Console.ReadKey();
                StringLengthRange = new StringLengthRange(min.Value, max.Value);
                return PostInvokeCommand.StayInSubmenu;
            }
        }

        private static class Mapper
        {
            private static readonly IReadOnlyDictionary<Type, Type> TypeRangeTypeMapper = new Dictionary<Type, Type>
            {
                { typeof(DateTime), typeof(DateRange) },
                { typeof(decimal), typeof(DecimalRange) },
                { typeof(double), typeof(DoubleRange) },
                { typeof(string), typeof(UIntRange) },
                { typeof(int), typeof(IntRange) },
                { typeof(TimeSpan), typeof(TimeSpanRange) },
                { typeof(uint), typeof(UIntRange) }
            };

            private static readonly IReadOnlyDictionary<Type, Func<string, object>> TypeGetValueMapper = new Dictionary<Type, Func<string, object>>
            {
                { typeof(DateTime), prompt => InputService.GetDate(prompt, DateRange) },
                { typeof(decimal), prompt => InputService.GetDecimal(prompt, DecimalRange) },
                { typeof(double), prompt => InputService.GetDouble(prompt, DoubleRange) },
                { typeof(int), prompt => InputService.GetInt(prompt, IntRange) },
                { typeof(string), prompt => InputService.GetString(prompt, StringLengthRange, AllowNewLine, AllowNull) },
                { typeof(TimeSpan), prompt => InputService.GetTimeSpan(prompt, TimeSpanRange) },
                { typeof(uint), prompt => InputService.GetUInt(prompt, UIntRange) }
            };

            private static readonly IReadOnlyDictionary<Type, Func<string, object>> TypeGetNullableValueMapper = new Dictionary<Type, Func<string, object>>
            {
                { typeof(DateTime), prompt => InputService.GetNullableDate(prompt, DateRange) },
                { typeof(decimal), prompt => InputService.GetNullableDecimal(prompt, DecimalRange) },
                { typeof(double), prompt => InputService.GetNullableDouble(prompt, DoubleRange) },
                { typeof(int), prompt => InputService.GetNullableInt(prompt, IntRange) },
                { typeof(TimeSpan), prompt => InputService.GetNullableTimeSpan(prompt, TimeSpanRange) },
                { typeof(uint), prompt => InputService.GetNullableUInt(prompt, UIntRange) }
            };

            private static readonly IReadOnlyDictionary<Type, Action<object>> TypeSetRangeActionMapper = new Dictionary<Type, Action<object>>
            {
                { typeof(DateTime), o => DateRange = (DateRange) o },
                { typeof(decimal), o => DecimalRange = (DecimalRange) o },
                { typeof(double), o => DoubleRange = (DoubleRange) o },
                { typeof(int), o => IntRange = (IntRange) o },
                { typeof(TimeSpan), o => TimeSpanRange = (TimeSpanRange) o },
                { typeof(uint), o => UIntRange = (UIntRange) o }
            };

            private static readonly IReadOnlyDictionary<Type, object> TypeDefaultMinValueMapper = new Dictionary<Type, object>
            {
                { typeof(DateTime), DateTime.MinValue },
                { typeof(decimal), decimal.MinValue },
                { typeof(double), double.MinValue },
                { typeof(int), int.MinValue },
                { typeof(TimeSpan), TimeSpan.MinValue },
                { typeof(uint), uint.MinValue }
            };

            private static readonly IReadOnlyDictionary<Type, object> TypeDefaultMaxValueMapper = new Dictionary<Type, object>
            {
                { typeof(DateTime), DateTime.MaxValue },
                { typeof(decimal), decimal.MaxValue },
                { typeof(double), double.MaxValue },
                { typeof(int), int.MaxValue },
                { typeof(TimeSpan), TimeSpan.MaxValue },
                { typeof(uint), uint.MaxValue }
            };

            private static readonly IReadOnlyDictionary<Type, string> TypeValueTypeNameMapper = new Dictionary<Type, string>
            {
                { typeof(DateTime), "Date" },
                { typeof(decimal), "Decimal" },
                { typeof(double), "Double" },
                { typeof(int), "Int" },
                { typeof(string), "String" },
                { typeof(TimeSpan), "Time span" },
                { typeof(uint), "UInt" }
            };

            public static Type GetRangeTypeFor<TValue>()
                where TValue : struct => TypeRangeTypeMapper[typeof(TValue)];

            public static Func<string, object> GetValueFuncFor<TValue>() => TypeGetValueMapper[typeof(TValue)];

            public static Func<string, object> GetNullableValueFuncFor<TValue>()
                where TValue : struct => TypeGetNullableValueMapper[typeof(TValue)];

            public static Action<object> GetSetRangeActionFor<TValue>()
                where TValue : struct => TypeSetRangeActionMapper[typeof(TValue)];
            public static object GetDefaultMinValueOf<TValue>()
                where TValue : struct => TypeDefaultMinValueMapper[typeof(TValue)];

            public static object GetDefaultMaxValueOf<TValue>()
                where TValue : struct => TypeDefaultMaxValueMapper[typeof(TValue)];

            public static string GetNameOf<TValue>() => TypeValueTypeNameMapper[typeof(TValue)];
        }
    }
}